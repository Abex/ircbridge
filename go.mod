module bitbucket.org/abex/ircbridge

go 1.14

require (
	gitlab.com/yumeko/config v0.0.0-20170717100451-96ea79a7943b
	gitlab.com/yumeko/errors v0.0.0-20170706094518-6c845cf45c2e // indirect
	golang.org/x/net v0.0.0-20200421231249-e086a090c8fd
	gopkg.in/ini.v1 v1.55.0 // indirect
	gopkg.in/sorcix/irc.v2 v2.0.0-20190306112350-8d7a73540b90
	layeh.com/gumble v0.0.0-20200325144729-ff9dcce1c140
)
