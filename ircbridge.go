package main

import (
	"bytes"
	"context"
	"crypto/tls"
	"fmt"
	"io"
	"net"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"runtime/pprof"
	"strings"
	"time"

	"golang.org/x/net/html"

	"layeh.com/gumble/gumble"
	"layeh.com/gumble/gumbleutil"

	"gopkg.in/sorcix/irc.v2"

	"encoding/json"

	"regexp"

	"gitlab.com/yumeko/config"
	configlistener "gitlab.com/yumeko/config/listener"
	"gitlab.com/yumeko/config/sighup"
)

var configINI = config.NewINI()
var configINIPath = config.NewString("ircs.ini", config.NewMTime(configINI))
var configRoot = config.NewInstance(configINIPath)
var yumekoRoot *url.URL
var sname string
var mhost string
var mpassword string
var mtokens []string
var minsecure bool

type ircServer struct{}

func (i *ircServer) Serve(l net.Listener) error {
	for {
		conn, err := l.Accept()
		if err != nil {
			return err
		}
		go handleIRCConn(conn)
	}
}
func (i *ircServer) Shutdown(context.Context) error {
	return nil
}

type ircDebug struct {
	irc.Conn
}

func (i *ircDebug) Decode() (*irc.Message, error) {
	m, e := i.Conn.Decode()
	if e == nil && m != nil {
		fmt.Printf(">%v\n", m.String())
	}
	return m, e
}
func (i *ircDebug) Encode(m *irc.Message) error {
	fmt.Printf("<%v\n", m.String())
	return i.Conn.Encode(m)
}

var logSourceMatcher = regexp.MustCompile(`<a[^>]+href=["']clientid[^>]+>([^<]*)<\/a>:?`)

func handleIRCConn(nconn net.Conn) {
	// conn := &ircDebug{}
	conn := *irc.NewConn(nconn)
	defer conn.Close()
	err := func() error {
		/*defer func() {
			err := recover()
			if err != nil {
				fmt.Println(err)
			}
		}()*/
		nick := ""
		authed := false
		user := ""
		nconn.SetReadDeadline(time.Now().Add(time.Minute))
		for !authed || user == "" || nick == "" {
			msg, err := conn.Decode()
			if msg == nil {
				fmt.Printf("bad message")
				return fmt.Errorf("invalid message")
			}
			if err != nil {
				return err
			}
			switch msg.Command {
			case "PASS":
				err := func() error {
					uri, _ := url.Parse("api/user")
					uri = yumekoRoot.ResolveReference(uri)
					q := uri.Query()
					q.Set("key", msg.Params[0])
					uri.RawQuery = q.Encode()
					res, err := http.Get(uri.String())
					if err != nil {
						return err
					}
					defer res.Body.Close()
					um := struct {
						Success bool `json:"success"`
					}{}
					err = json.NewDecoder(res.Body).Decode(&um)
					if err != nil {
						return err
					}
					if !um.Success {
						return fmt.Errorf("password must be yumeko token")
					}
					authed = true
					return nil
				}()
				if err != nil {
					return err
				}
			case "NICK":
				nick = msg.Params[0]
			case "USER":
				user = msg.Params[0]
			default:
			}
		}
		spfx := &irc.Prefix{
			Name: sname,
		}
		cpfx := &irc.Prefix{
			Name: nick,
			User: user,
			Host: "fake.net",
		}
		gcfg := gumble.NewConfig()
		gcfg.Username = DemangleName(nick)
		gcfg.Password = mpassword
		gcfg.Tokens = gumble.AccessTokens(mtokens)
		connected := make(chan struct{}, 1)
		gcfg.Attach(gumbleutil.Listener{
			Disconnect: func(ev *gumble.DisconnectEvent) {
				conn.Encode(&irc.Message{
					Prefix:  spfx,
					Command: irc.KILL,
					Params:  []string{nick, ev.String},
				})
				conn.Close()
			},
			UserChange: func(ev *gumble.UserChangeEvent) {
				if ev.Type.Has(gumble.UserChangeChannel) {
					select {
					case connected <- struct{}{}:
					default:
					}
				}
			},
		})
		mc, err := gumble.DialWithDialer(&net.Dialer{}, mhost, gcfg, &tls.Config{
			InsecureSkipVerify: minsecure,
		})
		if err != nil {
			return err
		}
		defer mc.Disconnect()
		channels := map[uint32]int{}
		for _, u := range mc.Users {
			channels[u.Channel.ID] = channels[u.Channel.ID] + 1
		}
		cid := uint32(0)
		idl := 0
		for id, c := range channels {
			if c > idl {
				idl = c
				cid = id
			}
		}
		channel := mc.Channels[cid]
		mc.Self.Move(channel)
		err = conn.Encode(&irc.Message{
			Prefix:  spfx,
			Command: irc.RPL_WELCOME,
			Params:  []string{nick, fmt.Sprintf("Welcome to the %v Network %v!%v@fake.net", sname, nick, user)},
		})
		if err != nil {
			return err
		}
		err = conn.Encode(&irc.Message{
			Prefix:  spfx,
			Command: irc.RPL_YOURHOST,
			Params:  []string{nick, fmt.Sprintf("Your host is %v, running version ircbridge", "fake.net")},
		})
		if err != nil {
			return err
		}
		err = conn.Encode(&irc.Message{
			Prefix:  spfx,
			Command: irc.RPL_CREATED,
			Params:  []string{nick, "This server was created just for you."},
		})
		if err != nil {
			return err
		}
		err = conn.Encode(&irc.Message{
			Prefix:  spfx,
			Command: irc.RPL_MYINFO,
			Params:  []string{nick, sname, "ircbridge", "o", "n"},
		})
		if err != nil {
			return err
		}
		err = conn.Encode(&irc.Message{
			Prefix:  spfx,
			Command: irc.RPL_ISUPPORT,
			Params:  []string{nick, "CASEMAPPING=rfc7613", "CHANLIMIT=1"},
		})
		if err != nil {
			return err
		}
		err = conn.Encode(&irc.Message{
			Prefix:  spfx,
			Command: irc.RPL_LUSERCLIENT,
			Params:  []string{nick, fmt.Sprintf("There are %v users and 0 invisible on 1 servers", len(channel.Users))},
		})
		if err != nil {
			return err
		}

		err = conn.Encode(&irc.Message{
			Prefix:  spfx,
			Command: irc.ERR_NOMOTD,
			Params:  []string{nick, ""},
		})
		if err != nil {
			return err
		}
		err = conn.Encode(&irc.Message{
			Prefix:  cpfx,
			Command: irc.JOIN,
			Params:  []string{"#mumble"},
		})
		if err != nil {
			return err
		}
		<-connected
		userlist := []string{nick}
		for _, u := range channel.Users {
			userlist = append(userlist, MangleName(u.Name).Name)
		}
		mc.Config.Attach(gumbleutil.Listener{
			TextMessage: func(msg *gumble.TextMessageEvent) {
				sender := ""
				if msg.Sender != nil {
					sender = msg.Sender.Name
				}
				mtext := msg.Message
				lsm := logSourceMatcher.FindStringSubmatch(mtext)
				if len(lsm) == 2 {
					sender = lsm[1]
					mtext = mtext[len(lsm[0]):]
				}
				ircm := &irc.Message{
					Prefix:  spfx,
					Command: irc.NOTICE,
					Params:  []string{"#mumble", ""},
				}
				if sender != "" {
					ircm.Prefix = MangleName(sender)
					ircm.Command = irc.PRIVMSG
				}
				if len(msg.Users) > 0 {
					ircm.Params[0] = ircm.Prefix.String()
				}
				irctxts := HTMLtoIRC(mtext, 510-ircm.Len())
				for _, irctxt := range irctxts {
					ircm.Params[1] = irctxt
					conn.Encode(ircm)
				}
			},
			UserChange: func(ev *gumble.UserChangeEvent) {
				joining := false
				leaving := false
				if ev.Type.Has(gumble.UserChangeChannel) {
					joining = ev.User.Channel == mc.Self.Channel
					leaving = !joining
				}
				if ev.Type.Has(gumble.UserChangeConnected) {
					joining = true
				}
				if ev.Type.Has(gumble.UserChangeDisconnected) || ev.Type.Has(gumble.UserChangeKicked) {
					leaving = true
				}
				if ev.User != mc.Self { //TODO: part and join the new userlist
					if joining {
						err = conn.Encode(&irc.Message{
							Prefix:  MangleName(ev.User.Name),
							Command: irc.JOIN,
							Params:  []string{"#mumble"},
						})
					}
					if leaving {
						conn.Encode(&irc.Message{
							Prefix:  MangleName(ev.User.Name),
							Command: irc.PART,
							Params:  []string{"#mumble"},
						})
					}
				}
				if ev.Type.Has(gumble.UserChangeAudio) {
					m := "+o"
					if isAFK(ev.User) {
						m = "-o"
					}
					pfx := MangleName(ev.User.Name)
					conn.Encode(&irc.Message{
						Prefix:  pfx,
						Command: irc.MODE,
						Params:  []string{"#mumble", m, pfx.Name},
					})
				}
			},
		})
		err = conn.Encode(&irc.Message{
			Prefix:  spfx,
			Command: irc.RPL_NAMREPLY,
			Params:  []string{nick, "=", "#mumble", strings.Join(userlist, " ")},
		})
		if err != nil {
			return err
		}
		for _, u := range channel.Users {
			if !isAFK(u) {
				pfx := MangleName(u.Name)
				conn.Encode(&irc.Message{
					Prefix:  pfx,
					Command: irc.MODE,
					Params:  []string{"#mumble", "+o", pfx.Name},
				})
			}
		}
		err = conn.Encode(&irc.Message{
			Prefix:  spfx,
			Command: irc.RPL_NOTOPIC,
			Params:  []string{"#mumble", "No topic is set"},
		})
		if err != nil {
			return err
		}
		//This frees comments, ~2mb of ram/connection
		for _, c := range mc.Channels {
			c.Description = ""
			c.DescriptionHash = nil
		}
		/*
			err = conn.Encode(&irc.Message{
				Prefix:  spfx,
				Command: irc.,
				Params:  []string{},
			})
			if err != nil {
				return err
			}
		*/
		pingout := false
		for {
			nconn.SetReadDeadline(time.Now().Add(time.Minute))
			msg, err := conn.Decode()
			if err != nil {
				nerr, ok := err.(net.Error)
				if ok && nerr.Timeout() && !pingout {
					err = conn.Encode(&irc.Message{
						Command: irc.PING,
						Params:  []string{sname},
					})
					continue
				}
				return err
			}
			switch msg.Command {
			case "JOIN":
			//nothing
			case "QUIT", "PART":
				return nil
			case "PING":
				err = conn.Encode(&irc.Message{
					Prefix:  spfx,
					Command: irc.PONG,
					Params:  []string{nick, msg.Params[0]},
				})
				if err != nil {
					return err
				}
			case "PRIVMSG":
				target := msg.Params[0]
				if len(target) < 0 || target[0] != '#' {
					pfx := irc.ParsePrefix(target)
					user := mc.Users.Find(DemangleName(pfx.Name))
					if user != nil {
						user.Send(msg.Params[1])
						continue
					}
				}
				mc.Self.Channel.Send(IRCtoHTML(msg.Params[1]), false)
			case "MODE":
				target := msg.Params[0]
				omsg := &irc.Message{
					Prefix: spfx,
					Params: []string{target, ""},
				}
				if len(target) > 0 && target[0] == '#' {
					if target == "#mumble" {
						omsg.Command = irc.RPL_CHANNELMODEIS
					} else {
						omsg.Command = irc.ERR_NOSUCHCHANNEL
						omsg.Params[1] = "No such channel"
					}
				} else {
					pfx := irc.ParsePrefix(target)
					user := mc.Users.Find(DemangleName(pfx.Name))
					if user != nil {
						omsg.Command = irc.RPL_UMODEIS
						if !isAFK(user) {
							omsg.Params[0] = "o"
						}
						omsg.Params = omsg.Params[:1]
					} else {
						omsg.Command = irc.ERR_NOSUCHNICK
						omsg.Params[1] = "No such nick/channel"
					}
				}
				conn.Encode(omsg)
			case "WHO":
				send := [][]string{}
				if msg.Params[0] == "#mumble" {
					for _, user := range mc.Self.Channel.Users {
						status := "H"
						if user.SelfMuted || user.Muted {
							status = "G"
						}
						mu := MangleName(user.Name)
						send = append(send, []string{
							"#mumble", mu.User, mu.Host, "fake.net", mu.Name, status, "0 " + stripCC.Replace(user.Name),
						})
					}
				} else {
					pfx := irc.ParsePrefix(msg.Params[0])
					user := mc.Users.Find(DemangleName(pfx.Name))
					if user != nil {
						status := "H"
						if user.SelfMuted || user.Muted {
							status = "G"
						}
						mu := MangleName(user.Name)
						send = append(send, []string{
							"#mumble", mu.User, mu.Host, "fake.net", mu.Name, status, "0 " + stripCC.Replace(user.Name),
						})
					}
				}
				if len(send) > 0 {
					for _, v := range send {
						conn.Encode(&irc.Message{
							Prefix:  spfx,
							Command: irc.RPL_WHOREPLY,
							Params:  v,
						})
					}
					conn.Encode(&irc.Message{
						Prefix:  spfx,
						Command: irc.RPL_ENDOFWHO,
						Params:  []string{nick, "End of /WHO list"},
					})
				} else {
					conn.Encode(&irc.Message{
						Prefix:  spfx,
						Command: irc.ERR_NOSUCHSERVER,
						Params:  []string{msg.Params[0], "No such server"},
					})
				}
			case "AWAY":
				//There is so much wrong here
				if mc.Self.SelfDeafened {
					mc.Self.Deafened = false
					mc.Self.SetSelfMuted(false)
				} else {
					mc.Self.Muted = true
					mc.Self.SetSelfDeafened(true)
				}
			}
		}
	}()
	if err != nil && err != io.EOF {
		conn.Encode(&irc.Message{
			Command: irc.ERROR,
			Params: []string{
				fmt.Sprintf("Error: %v", err),
			},
		})
	}
}

func init() {
	configINI.Attach(`[mumble]
# host:port to connect to mumble with
host = mumble:64738`, config.NewPointer(&mhost))
	configINI.Attach(`[mumble]
# Password to connect to mumble with
password =`, config.NewPointer(&mpassword))
	configINI.Attach(`[mumble]
# Access tokens to use to connect to mumble
tokens =`, config.NewPointer(func(s string) error {
		mtokens = strings.Split(s, " ")
		return nil
	}))
	configINI.Attach(`[mumble]
# Don't verify the certificate presented by the server
insecure = true`, config.NewPointer(&minsecure))
	configINI.Attach(`
# URL to connect to yumeko
yumeko = http://yumeko/`, config.NewPointer(&yumekoRoot))
	configINI.Attach(`[irc]
# Server name to show to clients
server-name = ircbridge.example.net`, config.NewPointer(&sname))
	configINI.Attach(`[irc]
# [host]:port to listen on for non secure client connections
irc = :6667`, configlistener.NewNet("tcp", true, func() (configlistener.Server, error) {
		return &ircServer{}, nil
	}))
	ircs := configlistener.NewTLS(true, func() (configlistener.Server, *tls.Config, error) {
		return &ircServer{}, &tls.Config{}, nil
	})
	configINI.Attach(`[irc]
# [host]:port to listen on for secure irc connections
ircs = :6697`, ircs.HostPort)
	configINI.Attach(`[irc]
# name of the certificate file for ircs
ircs_cert = irc.crt`, config.NewMTime(ircs.CertFile))
	configINI.Attach(`[irc]
# name of the certificate key file for ircs
ircs_keyt = irc.key`, config.NewMTime(ircs.KeyFile))
}

func isAFK(u *gumble.User) bool {
	return u.SelfMuted || u.SelfDeafened || u.Muted || u.Deafened
}

var replaces = [][2]rune{
	{'(', '('},
	{'@', 'a'},
	{'!', '1'},
	{' ', '_'},
	{'\000', '0'},
	{'\b', 'b'},
	{'\r', 'r'},
	{'\n', 'n'},
	{':', 'c'},
	{';', 's'},
	{'#', 'h'},
}

var stripCC = strings.NewReplacer("\b", "", "\r", "", "\n", "")

func MangleName(full string) *irc.Prefix {
	in := []rune(full)
	out := []rune{}
outer:
	for _, r := range in {
		for _, p := range replaces {
			if r == p[0] {
				out = append(out, '(', p[1], ')')
				continue outer
			}
		}
		out = append(out, r)
	}
	return &irc.Prefix{
		Name: string(out),
		User: string(out),
		Host: "mumble",
	}
}

var demangleMatcher = regexp.MustCompile(`(__|\()(.)(\)|__)`)

func DemangleName(mangled string) string {
	return demangleMatcher.ReplaceAllStringFunc(mangled, func(s string) string {
		for _, p := range replaces {
			c := s[len(s)/2]
			if p[1] == rune(c) {
				return string([]rune{p[0]})
			}
		}
		return "?"
	})
}

var linkMatcher = regexp.MustCompile(`([a-z-]+://[^ <]*)`)

func IRCtoHTML(s string) string {
	return linkMatcher.ReplaceAllString(s, `<a href="$0">$0</a>`)
}

const (
	cBold      = 0x02
	cItalics   = 0x1D
	cUnderline = 0x1F
)

func HTMLtoIRC(s string, maxlen int) []string {
	cl := &bytes.Buffer{}
	out := []string{}
	maxlen--
	tokStream := html.NewTokenizer(bytes.NewBufferString(s))
	pre := false
	bold := 0
	italics := 0
	underline := 0
	newline := func() {
		if cl.Len() > 0 {
			// Always append a space to the message is always a trailer
			// Easy enough solution. AtomicIRC breaks otherwise
			cl.WriteByte(' ')
			str := cl.String()
			for len(str) > 0 {
				ll := len(str)
				if ll > maxlen {
					ll = maxlen
				}
				out = append(out, str[:ll])
				str = str[ll:]
			}
			cl.Reset()
			if bold > 0 {
				cl.WriteByte(cBold)
			}
			if italics > 0 {
				cl.WriteByte(cItalics)
			}
			if underline > 0 {
				cl.WriteByte(cUnderline)
			}
		}
	}
loop:
	for {
		tokType := tokStream.Next()
		switch tokType {
		case html.ErrorToken:
			break loop
		case html.TextToken:
			b := tokStream.Text()
			if pre {
				start := 0
				for i, c := range b {
					if c == '\n' || c == '\r' {
						cl.Write(b[start:i])
						start = i + 1
						newline()
					}
					if c == 0 {
						b[i] = ' '
					}
				}
				cl.Write(b[start:len(b)])
				newline()
			} else {
				lastSpace := true
				end := 0
				for i := 0; i < len(b); i++ {
					c := b[i]
					if c == '\n' || c == '\r' || c == 0 {
						c = ' '
						b[i] = c
					}
					if !(c == ' ' && lastSpace) {
						b[end] = c
						end++
					}
					lastSpace = c == ' '
				}
				cl.Write(b[:end])
			}
		case html.StartTagToken, html.SelfClosingTagToken:
			name, hasAttr := tokStream.TagName()
			s := string(name)
			switch s {
			case "br", "dl", "dt", "p", "blockquote", "dd", "div", "h1", "h2", "h3", "h4", "h5", "h6", "hr", "li", "td":
				newline()
			case "pre":
				newline()
				pre = true
			case "address", "cite", "dfn", "em", "i", "var":
				if italics == 0 {
					cl.WriteByte(cItalics)
				}
				italics++
			case "th":
				newline()
				fallthrough
			case "b", "strong":
				if bold == 0 {
					cl.WriteByte(cBold)
				}
				bold++
			//case "s":
			case "u":
				if underline == 0 {
					cl.WriteByte(cUnderline)
				}
				underline++
			case "a":
				if hasAttr {
					for {
						key, value, more := tokStream.TagAttr()
						if bytes.Equal(key, []byte("href")) {
							newline()
							cl.Write(value)
							newline()
						}
						if !more {
							break
						}
					}
				}
			}
		case html.EndTagToken:
			name, _ := tokStream.TagName()
			s := string(name)
			switch s {
			case "blockquote", "dd", "div", "h1", "h2", "h3", "h4", "h5", "h6", "hr", "li", "td":
				newline()
			case "pre":
				newline()
				pre = false
			case "address", "cite", "dfn", "em", "i", "var":
				italics--
				if italics == 0 {
					cl.WriteByte(cItalics)
				}
			case "th":
				newline()
				fallthrough
			case "b", "strong":
				bold--
				if bold == 0 {
					cl.WriteByte(cBold)
				}
			//case "s":
			case "u":
				underline--
				if underline == 0 {
					cl.WriteByte(cUnderline)
				}
			}
		}
	}
	newline()
	return out
}

func main() {
	if len(os.Args) > 1 {
		configINIPath.Change(os.Args[1])
	}
	errs := configRoot.Reload()
	if len(errs) > 0 {
		fmt.Println("Fatal error:")
		for _, err := range errs {
			fmt.Println(err)
		}
		return
	}
	sighup.EnableSigHUP(configRoot, true)

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	<-c
	pprof.Lookup("goroutine").WriteTo(os.Stdout, 1)
}
